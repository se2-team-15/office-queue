package com.polito.officequeue.dto;


public class SimpleCounterID {
   private Integer counterId;
  
   public void setCounterId(Integer counterDto) {
	   this.counterId = counterDto;
   }
   
   public Integer getCounterId() {
	  return this.counterId;
   }
   
}
